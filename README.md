#### 一、项目简介
+ 一直以来都有一个社交梦，想做一款IM应用，看了很多优秀的开源项目，但是没有合适的。于是利用空闲休息时间自己写了这么一套系统。
+ 页面设计暂时借鉴`微信`的UI，后期会出一款自己的UI。
+ 目前手机端使用uniapp实现，仅这次`安卓`和`iOS`端，后期支持人数多，会继续实现桌面版和web版。
+ 项目第一版历时2个月，前端使用`uniapp`，后端使用`SpringBoot`。
+ 您的支持，就是我们`【生发的动力】`。
+ 前端源码地址：[https://gitee.com/lakaola/im-uniapp](https://gitee.com/lakaola/im-uniapp)
+ 后端源码地址：[https://gitee.com/lakaola/im-platform](https://gitee.com/lakaola/im-platform)

#### 二、技术使用
+ 消息推送：个推（后端对离线消息暂存60天）
+ 资源：阿里OSS（图片、文件、声音等）
+ 实时音视频：TRTC
+ 地图：高德地图
+ 短信：阿里云短信

#### 三、内置功能
+ 单聊
+ 群聊
+ 朋友圈
+ 附近的人
+ 收藏
+ 扫码加好友/加群
+ 聊天支持：文字、表情、图片、名片、收藏、声音、实时音视频等

#### 四、在线体验
+ [https://im.q3z3.com/](https://im.q3z3.com/)   
+ 暂时只支持安卓端体验
+ iOS端体验需要等到下个版本，换完UI，应用商店上架。

#### 五、演示效果
<img src="https://img.alicdn.com/imgextra/i2/87413133/O1CN017NSpYx1Z0xcvYxEbO_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i1/87413133/O1CN01Oq8SLw1Z0xcukUdV0_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i2/87413133/O1CN01mD2wwN1Z0xctYYdAA_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i3/87413133/O1CN01bZSz2q1Z0xco96F1t_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i3/87413133/O1CN01Pe8G6S1Z0xcmQluDI_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i1/87413133/O1CN012JP8VW1Z0xccuWKzM_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i4/87413133/O1CN01fMUNJA1Z0xck1w0kt_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i3/87413133/O1CN01n8MZhZ1Z0xctYZEbM_!!87413133.jpg" width="200">
<img src="https://img.alicdn.com/imgextra/i2/87413133/O1CN017vPSbK1Z0xcqoFn8E_!!87413133.jpg" width="200">

#### 六、请作者喝杯茶吧
<img src="https://img.alicdn.com/imgextra/i3/87413133/O1CN01Ilrbqk1Z0xcwW5PsK_!!87413133.jpg" width="600">

#### 七、未来计划（计划6月1日之后重启项目）
+ 更换手机端UI，目前使用微信UI
+ 适配iOS端
+ 适配Windows端
+ 增加通知栏消息展示（当app后台挂起，通知栏展示消息）
+ 各大应用市场上架
+ 后端优化，支持大并发
+ ...



